# EtabtoPsi2SJpsi_officialMC

Etab -> Psi(2S) Jpsi analysis. 

We consider two channels:

Channel 1: Psi(2S) and Jpsi will both decay to two muon. So the the final state is 4 muon.

Channel 2: Psi(2S) decays to Jpsi pi pi and then the both Jpsi from Psi(2S) and directly from etab will decay to two muons. So the final state is 4 muon and 2 pion.

The fragment files for the two channels are stored in two branches now.

## Psi(2S) to Jpsi pi pi channel

Fragment files of Psi(2S) to Jpsi pi pi channel will be stored in this branch(master).


### Signal MC

Scripts in Psi2StoJpsipipi/Signal dir 

The Filter efficiency (event-level) is 9.450e-02.

For each year, as discussed, we require 2e4 events(after GEN).

For both channels, we need UL MC samples for 2016, 2017 and 2018. The tool we use is Pythia8. This script should be used under CMSSW_10_6_20_patch1. 

We also need to go though rest steps, for example DIGIRAW, HLT and RECO


### Background MC

We have two background conponents: DPS and SPS.

#### DPS

Scripts in Psi2StoJpsipipi/Bkg/DPS dir

Fragment_DPStoPsi2SJ_Jpp_2016.py Fragment_DPStoPsi2SJ_Jpp_2017.py Fragment_DPStoPsi2SJ_Jpp_2018.py are private MC generation of DPS background.

We need UL MC samples of DPS background for 2016, 2017 and 2018

This script should be used under CMSSW_10_6_20_patch1

The Filter efficiency (event-level) is 2.85e-4

For each year, as discussed, we require 5e6 events.

#### SPS

Scripts in Psi2StoJpsipipi/Bkg/SPS dir

Fragment_SPStoPsi2SJ_Jpp_2016.py Fragment_SPStoPsi2SJ_Jpp_2017.py Fragment_SPStoPsi2SJ_Jpp_2018.py are private MC generation of SPS background.

We need UL MC samples of SPS background for 2016, 2017 and 2018

This script should be used under CMSSW_10_6_20_patch1

The Filter efficiency (event-level) is 1.063e-01

For each year, as discussed, we require 1e6 events.


